#!/bin/bash

error() { echo "$*" 1>&2 ; exit 1; }

if which python3 >/dev/null ; then
    PYTHON=python3
elif which python >/dev/null ; then
    PYTHON=python
else
    error "Python installation not found (tried 'python' and 'python3')"
fi

"${PYTHON}" -m venv "$(pwd)/venv" || error "Failed to create venv using ${PYTHON}"

if [[ "$1" != "--no-venv" ]] ; then
    source venv/bin/activate
    pip install --upgrade pip
fi

pip install -r requirements.txt || error "Command 'Pip install -r requirements.txt' failed"

pip install -e ./

echo Type "'"source venv/bin/activate"'" before running "'"pip install ..."'"
