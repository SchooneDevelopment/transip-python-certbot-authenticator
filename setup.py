from setuptools import setup

setup(
    name='certbot-transip-dns',
    package='certbot_transip_dns.py',
    install_requires=[
        'certbot',
    ],
    entry_points={
        'certbot.plugins': [
            'transip_authenticator = certbot_transip_dns:Authenticator',
        ],
    },
)
