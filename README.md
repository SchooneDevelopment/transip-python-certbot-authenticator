# TransIP Python3 Certbot Authenticator (DNS)

Allows you to automatically create letsencrypt certificates with certbot when using TransIP for your domains DNS

### Usage (python3 venv, recommended)

Preparation:

- Run `./install.sh` to install the virtual environment with certbot and this dependency.
- Copy file `creds-example.ini`, and call it `creds.ini`. Edit the content of this file.
- Copy your private key from TransIP to this directory, and call it `privkey.pem`.

Add new certificate for domain (run as root):

- (Edit `creds.ini`)
- Run `source venv/bin/activate`
- Run `./certonly.sh -d example.com` (replace `example.com` to your domain name) to get your certificate.
- Run `./renew.sh -d example.com` (replace `example.com` to your domain name) to renew your certificate.

### Usage (global)

Preparation (run as root):

- Run `./install.sh --no-venv` to install certbot and this dependency.
- Copy file `creds-example.ini`, and call it `creds.ini`. Edit the content of this file.
- Copy your private key from TransIP to this directory, and call it `privkey.pem`.

Add new certificate for domain (run as root):

- (Edit `creds.ini`)
- Run `./certonly.sh -d example.com` (replace `example.com` to your domain name) to get your certificate.
- Run `./renew.sh -d example.com` (replace `example.com` to your domain name) to renew your certificate.
