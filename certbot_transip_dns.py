"""DNS Authenticator for TransIP."""
import logging
from typing import Any
from typing import Callable
from typing import Dict
from typing import List
from typing import Optional

import http.client
import urllib.parse
import json
import random
import sys
import re
import math
from OpenSSL import crypto
import binascii
import base64
import uuid
import struct
import time
from datetime import datetime

from certbot import errors
from certbot.plugins import dns_common
from certbot.plugins.dns_common import CredentialsConfiguration

logger = logging.getLogger(__name__)

class Authenticator(dns_common.DNSAuthenticator):
    """DNS Authenticator for TransIP

    This Authenticator uses the TransIP API to fulfill a dns-01 challenge.
    """

    description = ('Obtain certificates using a DNS TXT record (if you are using TransIP for '
                   'DNS).')
    ttl = 120

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.credentials: Optional[CredentialsConfiguration] = None

    @classmethod
    def add_parser_arguments(cls, add: Callable[..., None],
                             default_propagation_seconds: int = 10) -> None:
        super().add_parser_arguments(add, default_propagation_seconds)
        add('credentials', help='TransIP credentials INI file (--transip_authenticator-credentials <file>).')

    def more_info(self) -> str:
        return 'This plugin configures a DNS TXT record to respond to a dns-01 challenge using ' + \
               'the TransIP API.'

    def _validate_credentials(self, credentials: CredentialsConfiguration) -> None:
        private_key = credentials.conf('api-private-key-file')
        tld = credentials.conf('tld')
        login_name = credentials.conf('login-name')
        token_label_prefix = credentials.conf('token-label-prefix')

        if private_key and tld and login_name and token_label_prefix:
            pass
        else:
            raise errors.PluginError('"transip_authenticator_api_private_key_file=...", "transip_authenticator_tld=...", "transip_authenticator_login_name=..." and "transip_authenticator_token_label_prefix=..." are required in your .ini file. See file "creds-example.ini" for help.')

    def _setup_credentials(self) -> None:
        self.credentials = self._configure_credentials(
            'credentials',
            'TransIP credentials INI file',
            None,
            self._validate_credentials
        )

    def _perform(self, domain: str, validation_name: str, validation: str) -> None:
        self._get_transip_client().add_txt_record(domain, validation_name, validation, self.ttl)

    def _cleanup(self, domain: str, validation_name: str, validation: str) -> None:
        self._get_transip_client().del_txt_record(domain, validation_name, validation)

    def _get_transip_client(self) -> "_TransIPClient":
        if not self.credentials:
            raise errors.Error("Plugin has not been prepared.")
        return _TransIPClient(self.credentials.conf('api-private-key-file'), self.credentials.conf('tld'), self.credentials.conf('login-name'), self.credentials.conf('token-label-prefix'))


class _TransIPClient:
    """
    Encapsulates all communication with the TransIP API.
    """

    def __init__(self, private_key_file: str, tld: str, login_name: str, token_label_prefix: str) -> None:
        with open(private_key_file) as f:
            lines = f.readlines()
        lines = "".join(lines)
        while(lines[-1] == '\r' or lines[-1] == '\n'):
            lines = lines[:-1]
        
        self.private_key = lines
        self.token = None
        self.record_ttl = None
        self.tld = tld
        self.login_name = login_name
        self.token_label_prefix = token_label_prefix

    def request_access_token(self):
        now = datetime.now()
        
        sec = int(time.mktime(now.timetuple()))

        body = {
            'login': self.login_name,
            'nonce': self.uniqid(),
            'read_only': False,
            'expiration_time': "30 minutes",
            'label': self.token_label_prefix + str(sec),
            'global_key': True,
        }
        
        jsonVal = json.dumps(body)

        private_key = "";

        # Sign using OpenSSL
        if self.private_key.startswith("-----BEGIN PRIVATE KEY-----"):
            private_key = self.private_key[len("-----BEGIN PRIVATE KEY-----"):]
        elif  self.private_key.startswith("-----BEGIN RSA PRIVATE KEY-----"):
            private_key = self.private_key[len("-----BEGIN RSA PRIVATE KEY-----"):]
        else:
            raise errors.Error("Private key invalid format (begin).")

        if private_key.endswith("-----END PRIVATE KEY-----"):
            private_key = private_key[:-len("-----END PRIVATE KEY-----")]
        elif private_key.endswith("-----END RSA PRIVATE KEY-----"):
            private_key = private_key[:-len("-----END RSA PRIVATE KEY-----")]
        else:
            raise errors.Error("Private key invalid format (end).")
        
        private_key = re.sub(r"\s+", "", private_key)

        private_key2 = ""
        for i in range(0, math.ceil(len(private_key) / 64)):
            private_key2 += private_key[i*64:(i+1)*64] + "\n"
        private_key = private_key2

        private_key = "-----BEGIN PRIVATE KEY-----\n" + private_key + "-----END PRIVATE KEY-----"

        key = crypto.load_privatekey(crypto.FILETYPE_PEM, private_key)
        signature = crypto.sign(key, jsonVal.encode(), 'sha512')

        signature = base64.b64encode(signature).decode()

        headers = {
            'Content-Type': 'application/json',
            'Signature': signature
        }

        conn = http.client.HTTPSConnection("api.transip.nl")

        conn.request("POST", "/v6/auth", jsonVal, headers)

        response = conn.getresponse()
        return json.loads(response.read().decode())["token"]

    def add_txt_record(self, domain: str, record_name: str, record_content: str,
                       record_ttl: int) -> None:
        if not self.token:
            self.token = self.request_access_token()
        conn = http.client.HTTPSConnection("api.transip.nl")

        body = {
            "dnsEntry": {
                "name": record_name[:-(len(self.tld)+1)],
                "expire": 3600,
                "type": "TXT",
                "content": record_content
            }
        }
        jsonBody = json.dumps(body)

        headers = {
            "Authorization": "Bearer "  + self.token
        }

        conn.request("POST", "/v6/domains/" + urllib.parse.quote(self.tld, safe='') + "/dns", jsonBody, headers)
        response = conn.getresponse()
        response.read().decode()

    def del_txt_record(self, domain: str, record_name: str, record_content: str) -> None:
        if not self.token:
            self.token = self.request_access_token()
        conn = http.client.HTTPSConnection("api.transip.nl")

        body = {
            "dnsEntry": {
                "name": record_name[:-(len(self.tld)+1)],
                "expire": 3600,
                "type": "TXT",
                "content": record_content
            }
        }
        jsonBody = json.dumps(body)

        headers = {
            "Authorization": "Bearer "  + self.token
        }

        conn.request("DELETE", "/v6/domains/" + urllib.parse.quote(self.tld, safe='') + "/dns", jsonBody, headers)
        response = conn.getresponse()
        response.read().decode()

    def uniqid(self, prefix: str = "", more_entropy: bool = False):
        if more_entropy:
            raise errors.Error('more_entropy is not supported (yet)')
        
        now = datetime.now()
        
        sec = int(time.mktime(now.timetuple()))
        micro = now.microsecond % 0x100000
        
        uniqid = prefix + "%08x" % sec + "%05x" % micro

        return uniqid
