#!/bin/bash

certbot certonly --renew-by-default --authenticator transip_authenticator --transip_authenticator-credentials ./creds.ini $@
