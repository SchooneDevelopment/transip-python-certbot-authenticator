#!/bin/bash

certbot certonly --authenticator transip_authenticator --transip_authenticator-credentials ./creds.ini $@
